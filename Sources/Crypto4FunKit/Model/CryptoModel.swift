//
//  CryptoModel.swift
//  MyWallet
//
//  Created by Elliot Knight on 12/06/2022.
//

import Foundation
/*
 Documentation: https://docs.bitfine.com/reference/rest-public-tickers
 */

public struct CryptoCurrencyModel: Codable, Hashable {
	public let id, name: String
	public let image: String
	public let currentPrice: Double
	public let priceChangePercentage24h: Float

	enum CodingKeys: String, CodingKey {
		case id, name
		case image
		case currentPrice = "current_price"
		case priceChangePercentage24h = "price_change_percentage_24h"
	}
}
